# PCB Status and Planned Changes

* Controller:
  * Current board usable with header on same side as RJ45
  * Changes for version 1.10 boards:
    * Move silk screen for header to back side of board
    * Update version number

I2C-relay:
  * Current board usable with signal line fixes
  * Changes for version 1.10 boards:
    * Correct signal connections

Quadrature encoder:
  * Current board usable with resistors mounted on back side
  * Changes for version 1.10 boards:
    * Move silk screen for resistors to back side of board

Probe:
  * Current board needs no changes except addition of "armor":
    * Reflow solder 19mm squares of .010 thick copper brass over the six probe pads.

Shield:
  * Current board usable with I2C pullup workaround, but multiple requirements changes will make it obsolete with
  * Changes for version 1.10 boards:
   * Fix resistor pullups to 5v
   * Shift pullup resistors and jumpers away from
      mounting hole and USB connector
   * Change pullup resistors to 3k ohms
   * 100uF cap for neopixel decoupling, appropriate tplace
   * Shorter reset button
   * Fix rj45 swap error: quadrature to "left", controller vertical mount toward "bottom"
   * Add 3mm LED and resistor driven by pin D13
   * Remove DC barrel connector
   *  move FET away from mounting hole
   * Add Molex 0015244449 four pin connector (Digikey WM9132-ND)
   * Add optional snubber diode
   * Coil connection changes
   * Vertical mount controller connector moved away from mounting hole
   * Tplace circles marking mounting screw holes
