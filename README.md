# Arena-pcbs

Eagle and Kicad and other files relating to the IEEE Southeastcon 2017 robotics contest arena electronics PCBs. Main conference site [here](http://sites.ieee.org/southeastcon2017). Software and other CAD files [here](https://github.com/ncgadgetry/southeastcon2017).

Probe board left at rev 1.00 but with brass squares reflow-soldered to pads to withstand potentially sharp probes. 

Other boards at version 1.10, tested and ready, with copies available here:

* [Controller] (https://oshpark.com/shared_projects/pALyXufL)
* [I2C Relay/GPIO] (https://oshpark.com/shared_projects/CK8nXPIB)
* [Encoder] (https://oshpark.com/shared_projects/O3otCdHR)
* [Arduino Shield] (https://oshpark.com/shared_projects/eYJZTuTj)
 

## Boards

![Alt Text](/group-photo.jpg?raw=true "Arena PCBs")

---
### Stage 1 Probe Pads - Kicad

![Alt Text](/stage1-probe/probe-board-top.jpg?raw=true "Probe Pad Board Front")
![Alt Text](/stage1-probe/probe-board-bottom.jpg?raw=true "Probe Pad Board Back")

  * [Schematic PDF] (https://github.com/petesoper/arena-pcbs/blob/master/stage1-probe/probe-board-schematic.pdf)
  * [PCB Layout PDF] (https://github.com/petesoper/arena-pcbs/blob/master/stage1-probe/probe-board-layout.pdf)

  * Components
    * [L1 - Inductor Bourns JW Miller 70F501AF-RC 500mH, 30mA choke] (https://www.amazon.com/BOURNS-JW-MILLER-70F501AF-RC-CHOKE/dp/B011OBAT68/ref=sr_1_2?s=industrial&ie=UTF8&qid=1488381312&sr=1-2&keywords=miller+70F501AF-RC)
    * [Inductor data sheet] (http://datasheet.octopart.com/70F501AF-RC-Bourns-datasheet-8115212.pdf)
    * [C1 - Capacitor 70nF +/-20% 50V Ceramic] (https://www.amazon.com/gp/product/B008DFCUFW/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)
    * [R1 - Resistor 10k ohm 1/2W Carbon Film] (https://www.amazon.com/gp/product/B0185FIOTA/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)
    * [D1, D2 - Diodes 1N4001 50PIV Silicon] (https://www.amazon.com/gp/product/B00UPSVH36/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)
    * [JP1 - 2x7 male header Connection to relays & fuse - cut the linked-to 2x23 part to fit](https://www.sparkfun.com/products/12791)
    * [Brass sheet .010in thick .75x.75in reflow soldered to probe pads - cut with aircraft snips to lie *flat*, kapton tape edges](https://www.lowes.com/pd/The-Hillman-Group-4-in-x-0-42-ft-Brass-Sheet-Metal/3478153)

  * Notes
    * Board ready for use after this change: 3/4" squares of .010 thick brass sheet are reflow soldered over each of the six probe pads. Squares are cut with aircraft snips to lie flat over solder paste and held in place with kapton tape on two edges (do NOT cover all edges or flux vapors will rupture the tape connections and lift the sheet).

  * Versions
    * 1.00 - current

---
* Stage 1 I2C Board - Eagle

![Alt Text](/stage1-i2c/i2c-board-top.jpg?raw=true "I2C Relay Control Board Top View")
![Alt Text](/stage1-i2c/i2c-board-bottom.jpg?raw=true "I2C Relay Control Board Bottom View")

  * [Schematic PDF] (https://github.com/petesoper/arena-pcbs/blob/master/stage1-i2c/i2c-board-schematic.pdf)
  * [PCB Layout PDF] (https://github.com/petesoper/arena-pcbs/blob/master/stage1-i2c/i2c-board-layout.pdf)
  * [Iowa Scaled Engineering Model with CC-BY-SA used as starting point] (http://www.iascaled.com/store/I2C-RELAY16?filter_name=relay)

  * Components
    * [IC1 - PCA9671D 16 Bit I/O Expander I2C SOIC24] (http://datasheet.octopart.com/PCA9671D-Philips-datasheet-13264992.pdf)
    * [C1 - Capacitor 0.1uF 50V Ceramic] (https://www.amazon.com/gp/product/B008DFCUFW/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)
    * [C2 - 10uF 16V 5mm diameter Electrolytic Capacitor for decoupling](http://www.digikey.com/product-detail/en/nichicon/UVP1C100MDD1TD/493-12679-1-ND/4328296)
    * [JP4 - 2x10 female header connection to relay board](http://www.digikey.com/product-detail/en/sullins-connector-solutions/PPPC102LFBN-RC/S6106-ND/807245)
    * [JP1-JP3 1x3 male headers for selecting I2C chip address] (https://www.sparkfun.com/products/116)
    * [JP8 - 1x2 male header optionally powers board and bus from 2x10 header when NOT being used with a relay board] (https://www.sparkfun.com/products/116)
    * [JP9 - 1x2 male header default board power from bus] (https://www.sparkfun.com/products/116)
    * [JP5, JP6 - 2xRJ45 jack] (https://www.sparkfun.com/products/643)
    * 1/2 inch long nylon spacer and 3mm or 4/40 machine screw for holding I2C board to relay board

  * Notes
    * Passes all tests, I2C signals look good.

  * Versions
    * 1.00 - usuable with wires to correct bus connections
    * 1.10 - current

---
* Stage 2 Arduino Shield - Eagle

![Alt Text](/stage2-shield/arena-shield-top.jpg?raw=true "Connection Arduino Shield Top View")
![Alt Text](/stage2-shield/arena-shield-bottom.jpg?raw=true "Connection Arduino Shield Bottom View")

  * [Schematic PDF] (https://github.com/petesoper/arena-pcbs/blob/master/stage2-shield/arena-shield-schematic.pdf)
  * [PCB Layout PDF] (https://github.com/petesoper/arena-pcbs/blob/master/stage2-shield/arena-shield-layout.pdf)

  * [Hole Dimension Drawings] (https://blog.adafruit.com/2011/02/28/arduino-hole-dimensions-drawing/)
  * Components

    * NOTE WELL: Minimize the torque on the Molex connector on the shield board. Excellent soldering process should be used, with precleaning, liberal solder, and proper heating for good joints. The fit of the pins in the holes is not as snug as it should have been, but the fit of the mating connector is very snug. To remove the mate, wiggle it (not the wires) back and forth toward the narrow ends a little at a time while pulling with moderate force: NEVER GET IN A HURRY OR YANK. 
    * NOTE: The 100 watt, four ohm resistor and custom coil described in a later section should be mounted close to this shield, in series and connected to the screw terminal connector of the shield. This load can only be powered properly with a transistor or equivalent high current switch: it cannot be directly powered from a microcontroller.

    * [S1 - Momentary contact tactile switch - reset] (https://smile.amazon.com/gp/product/B008DS15ZA?sa-no-redirect=1)
    * [JP2 - 1x4 male header serial debug I/O, power and ground] (https://www.sparkfun.com/products/116)
    * [JP5 - 1x4 male header auxiliary GPIO, power and ground] (https://www.sparkfun.com/products/116)
    * [JP1 - 1x6 male header light saber, vibration sensor, power and ground] (https://www.sparkfun.com/products/116)
    * [JP7 - RJ45 jack - Controller/Display] (https://www.sparkfun.com/products/643) 
    * [JP8 - RJ45 jack - I2C relay I/O] (https://www.sparkfun.com/products/643)
    * [JP6 - RJ45 jack - Encoder/RGB] (https://www.sparkfun.com/products/643)
    * [JP4 - 1x2 3.5mm terminal block] (https://www.adafruit.com/products/724)
    * [JP9 - 2.1mm barrel jack PC Power for everything except relays and magnet coil] (https://www.sparkfun.com/products/119)
    * [Q1 - Power FET] (https://cdn-shop.adafruit.com/datasheets/irlb8721pbf.pdf)
    * [R1 - 100 Ohm 1/4w resistor controls FET rise time](http://www.digikey.com/product-detail/en/stackpole-electronics-inc/RNF14FTD100R/RNF14FTD100RCT-ND/1974980)
    * [R2 - 300 Ohm 1/4w resistor protects against neopixel controller inrush currents](http://www.digikey.com/product-search/en/resistors/through-hole-resistors/66690?k=&pkeyword=&pv7=2&pv1=414&pv3=1&pv2=4&FV=fff40001%2Cfff80482&mnonly=0&newproducts=0&ColumnSort=0&page=1&stock=1&quantity=0&ptm=0&fid=0&pageSize=25)
    * [R3, R4 - 3k Ohm 1/4w resistor I2C bus pullups](http://www.digikey.com/product-search/en/resistors/through-hole-resistors/66690?k=&pkeyword=&pv46=18241&FV=fff40001%2Cfff80482%2Cfffc02e2%2C40938%2C80004%2Cc0002%2C1c0002&mnonly=0&newproducts=0&ColumnSort=0&page=1&quantity=0&ptm=0&fid=0&pageSize=25)
    * [C2 - 100F 16v electrolytic cap decoupling for neopixel strings](http://www.digikey.com/product-detail/en/nichicon/UVP1C101MPD1TD/493-12680-1-ND/4328297)
    * [C1 - 10uf 16v or higher electrolytic cap decoupling for I2C bus power](http://www.digikey.com/product-detail/en/nichicon/UVP1C100MDD1TD/493-12679-1-ND/4328296)
    * [2 1x8, 2 1x6 long headers for shield connection to Arduino] (https://smile.amazon.com/Gikfun-2-54mm-Single-Breakaway-Arduino/dp/B00U8OCENY/ref=sr_1_4?s=pc&ie=UTF8&qid=1481137054&sr=1-4&keywords=breakaway+male+header)

  * Notes

    * Passes all tests.
    * ERRATA in 1.10 board silkscreen: Molex placement is backwards. Connector should be placed so a "standard" PC (IDE/CD/DVD) power connector has red corresponding to +5 and yellow to +12.
    * The Molex power connector is only held in place by its solder joints and it should be held while inserting/removing mating connector.
    * Be careful connecting the shield to the controller, I2C-relay, and encoder boards, as incorrect connection will stress the Arduino IO pins
    * The I2C bus pullups are designed to handle a maximum of 400pf of bus capacitance. CAT5 cables between shield and controller board and between shield and relay control around the contest arenas that are five feet long create loads of 150pf each, well within the spec.

  * Versions
    * 1.00 - usable, but encoder/controller labels backwards, no pin 13 LED, requires different coil connection
    * 1.10 - current

---
* Stage 3 Quadrature Encoder - Eagle

![Alt Text](/stage3-quadrature/encoder-board-top.jpg?raw=true "Quadrature Encoder Board Top View")
![Alt Text](/stage3-quadrature/encoder-board-bottom.jpg?raw=true "Quadrature Encoder Board Bottom View")

  * [Schematic PDF] (https://github.com/petesoper/arena-pcbs/blob/master/stage3-quadrature/encoder-board-schematic.pdf)
  * [PCB Layout PDF] (https://github.com/petesoper/arena-pcbs/blob/master/stage3-quadrature/encoder-board-layout.pdf)

  * Components

    * [SW2 - RGB LED Illuminated Rotary Encoder] (https://www.sparkfun.com/products/10982)
    * [Knob for Encoder] (https://www.sparkfun.com/products/10597)
    * [J1 - RJ45 jack - To Arduino Shield] (https://www.sparkfun.com/products/643)
    * [R1 150 ohm 1/4 watt resistor](http://www.digikey.com/product-detail/en/stackpole-electronics-inc/RNMF14FTC150R/S150CACT-ND/2617812)
    * [R2,R3 82 ohm 1/4 watt resistor](http://www.digikey.com/product-search/en/resistors/through-hole-resistors/66690?k=&pkeyword=&pv7=2&pv1=1122&pv3=1&pv2=4&FV=fff40001%2Cfff80482&mnonly=0&newproducts=0&ColumnSort=0&page=1&stock=1&quantity=0&ptm=0&fid=0&pageSize=25)

  * Notes

    * Passes all tests.

  * Versions
    * 1.00 - usable, but silk screen shows resistors on wrong side. Resistors must be on same side as RJ45 connector.
    * 1.10 - current

---
* Controller/Display - Eagle

![Alt Text](/controller/control-board-top.jpg?raw=true "Controller/Display Top View")
![Alt Text](/controller/control-board-bottom.jpg?raw=true "Controller/Display Bottom View")

  * [Schematic PDF] (https://github.com/petesoper/arena-pcbs/blob/master/controller/control-board-schematic.pdf)
  * [PCB Layout PDF] (https://github.com/petesoper/arena-pcbs/blob/master/controller/control-board-layout.pdf)

  * Components
    * [S1-S4 Momentary Contact Tactile Switch](https://smile.amazon.com/gp/product/B008DS15ZA?sa-no-redirect=1)
    * [JP1 1x4 Male Header to LCD] (https://www.sparkfun.com/products/116)
    * [JP2 RJ45 jack to Arduino Shield](https://www.sparkfun.com/products/643)
    * [4x20 LCD Display](https://www.amazon.com/gp/product/B0080DYTZQ)

  * Notes
    * Passes all tests. I2C signals look good.

  * Versions
    * 1.00 - usable
    * 1.10 - current

---
# Additional Components

  * [16 Channel 12V Relay Board] (http://www.sainsmart.com/16-channel-12v-relay-module-for-pic-arm-avr-dsp-arduino-msp430-ttl-logic.html)
  * 5 Volt 1.5 amp or greater DC Power Supply - Coil power
  * 12 Volt Two amp or greater DC Power Supply - Arduino, Relay Power
  * [4 ohm 100 watt power resistor](https://smile.amazon.com/gp/product/B00899WR32?sa-no-redirect=1)
  * [20 gauge wire coil as per contest rules description](https://www.pololu.com/product/2656)
  * [Arduino Uno or equivalent] (https://store-usa.arduino.cc/products/a000066)

---
# References

* [Contest Rules](http://sites.ieee.org/southeastcon2017/files/2016/10/MMXVII-October-9-release.pdf)
* Arduino connection map:
  *   D0:  (O) Rx
  *   D1:  (I) Tx
  *   D2:  (I) vibration sensor (IRQ)
  *   D3:  (I) quadrature channel B (IRQ)
  *   D4:  (I) quadrature channel A
  *   D5:  (I) quadrature pushbutton switch
  *   D6:  (O) neopixel stick data
  *   D7:  (O) LED enable
  *   D8:  (O) LED red (cathode)
  *   D9:  (O) LED green (cathode)
  *   D10: (O) LED blue (cathode)
  *   D11: (I/O) auxiliary I/O
  *   D12: (I/O) auxiliary I/O
  *   D13: (O) magnetic coil (and UNO on-board LED)
  *   GND: neopixel, vibration, quadrature common
  *   5v:  neopixel, LED common (anode)

