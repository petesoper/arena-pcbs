EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "IEEE Southeastcon 2017 Probe Pad Board"
Date "2016-11-12"
Rev "1.00"
Comp "pete@soper  SDevCS, LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R1
U 1 1 5783A808
P 7400 3050
F 0 "R1" V 7480 3050 50  0000 C CNN
F 1 "10k" V 7400 3050 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7330 3050 50  0001 C CNN
F 3 "" H 7400 3050 50  0000 C CNN
	1    7400 3050
	0    1    1    0   
$EndComp
$Comp
L D D1
U 1 1 5807C516
P 7100 3450
F 0 "D1" H 7100 3550 50  0000 C CNN
F 1 "1N4001" H 7100 3350 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-41_SOD81_Horizontal_RM10" H 7100 3450 50  0001 C CNN
F 3 "" H 7100 3450 50  0000 C CNN
	1    7100 3450
	1    0    0    -1  
$EndComp
$Comp
L D D2
U 1 1 5807C551
P 7525 3350
F 0 "D2" H 7525 3450 50  0000 C CNN
F 1 "1N4001" H 7525 3250 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-41_SOD81_Horizontal_RM10" H 7525 3350 50  0001 C CNN
F 3 "" H 7525 3350 50  0000 C CNN
	1    7525 3350
	-1   0    0    1   
$EndComp
$Comp
L C C1
U 1 1 5807E8D7
P 7800 3150
F 0 "C1" H 7825 3250 50  0000 L CNN
F 1 ".1uf" H 7825 3050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 7838 3000 50  0001 C CNN
F 3 "" H 7800 3150 50  0000 C CNN
	1    7800 3150
	0    1    1    0   
$EndComp
$Comp
L R2D2-PAD P4
U 1 1 58129706
P 4425 1400
F 0 "P4" H 4425 1675 60  0000 C CNN
F 1 "R2D2-PAD" H 4500 1125 60  0000 C CNN
F 2 "PJS-Footprints:R2D2-ROUND-PAD" H 4425 1400 60  0001 C CNN
F 3 "" H 4425 1400 60  0000 C CNN
	1    4425 1400
	1    0    0    -1  
$EndComp
$Comp
L R2D2-PAD P3
U 1 1 5812975E
P 3750 1400
F 0 "P3" H 3750 1675 60  0000 C CNN
F 1 "R2D2-PAD" H 3825 1125 60  0000 C CNN
F 2 "PJS-Footprints:R2D2-ROUND-PAD" H 3750 1400 60  0001 C CNN
F 3 "" H 3750 1400 60  0000 C CNN
	1    3750 1400
	1    0    0    -1  
$EndComp
$Comp
L R2D2-PAD P2
U 1 1 581297BD
P 3050 1425
F 0 "P2" H 3050 1700 60  0000 C CNN
F 1 "R2D2-PAD" H 3125 1150 60  0000 C CNN
F 2 "PJS-Footprints:R2D2-ROUND-PAD" H 3050 1425 60  0001 C CNN
F 3 "" H 3050 1425 60  0000 C CNN
	1    3050 1425
	1    0    0    -1  
$EndComp
$Comp
L R2D2-PAD P1
U 1 1 5812982B
P 2300 1425
F 0 "P1" H 2300 1700 60  0000 C CNN
F 1 "R2D2-PAD" H 2375 1150 60  0000 C CNN
F 2 "PJS-Footprints:R2D2-ROUND-PAD" H 2300 1425 60  0001 C CNN
F 3 "" H 2300 1425 60  0000 C CNN
	1    2300 1425
	1    0    0    -1  
$EndComp
$Comp
L R2D2-PAD P5
U 1 1 581299D6
P 5100 1425
F 0 "P5" H 5100 1700 60  0000 C CNN
F 1 "R2D2-PAD" H 5175 1150 60  0000 C CNN
F 2 "PJS-Footprints:R2D2-ROUND-PAD" H 5100 1425 60  0001 C CNN
F 3 "" H 5100 1425 60  0000 C CNN
	1    5100 1425
	1    0    0    -1  
$EndComp
Text Label 5900 3675 0    60   ~ 0
key
$Comp
L R2D2-PAD P6
U 1 1 5813B857
P 5800 1425
F 0 "P6" H 5800 1700 60  0000 C CNN
F 1 "R2D2-PAD" H 5875 1150 60  0000 C CNN
F 2 "PJS-Footprints:R2D2-ROUND-PAD" H 5800 1425 60  0001 C CNN
F 3 "" H 5800 1425 60  0000 C CNN
	1    5800 1425
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X07 JP1
U 1 1 5813C769
P 6350 3350
F 0 "JP1" H 6350 3750 50  0000 C CNN
F 1 "CONN_02X07" V 6350 3350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x07" H 6350 2150 50  0001 C CNN
F 3 "" H 6350 2150 50  0000 C CNN
	1    6350 3350
	1    0    0    -1  
$EndComp
Text Notes 6325 7000 0    110  ~ 0
Creative Commons Universal License (CCO 1.0)
Text Notes 6325 7200 0    100  ~ 0
https://creativecommons.org/publicdomain/zero/1.0
$Comp
L INDUCTOR L1
U 1 1 58266A03
P 7000 3250
F 0 "L1" V 6950 3250 50  0000 C CNN
F 1 "INDUCTOR" V 7100 3250 50  0000 C CNN
F 2 "PJS-Footprints:BigFatChoke" H 7000 3250 50  0001 C CNN
F 3 "" H 7000 3250 50  0000 C CNN
	1    7000 3250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8000 3650 6600 3650
Wire Wire Line
	6950 3650 6950 3550
Wire Wire Line
	6950 3550 6600 3550
Wire Wire Line
	6600 3450 6950 3450
Wire Wire Line
	6600 3350 7375 3350
Wire Wire Line
	6600 3250 6700 3250
Wire Wire Line
	6600 3150 7650 3150
Wire Wire Line
	6600 3050 7250 3050
Connection ~ 6950 3650
Wire Wire Line
	7250 3450 8000 3450
Connection ~ 8000 3450
Wire Wire Line
	7675 3350 8000 3350
Connection ~ 8000 3350
Wire Wire Line
	7950 3150 8000 3150
Connection ~ 8000 3150
Wire Wire Line
	7550 3050 8000 3050
Connection ~ 8000 3050
Wire Wire Line
	7300 3250 8000 3250
Connection ~ 8000 3250
Wire Wire Line
	8000 3250 8000 3650
Wire Wire Line
	8000 3050 8000 3275
Wire Wire Line
	5800 1825 5800 3050
Wire Wire Line
	5800 3050 6100 3050
Wire Wire Line
	5100 1825 5100 3150
Wire Wire Line
	5100 3150 6100 3150
Wire Wire Line
	4425 1800 4425 3250
Wire Wire Line
	4425 3250 6100 3250
Wire Wire Line
	3750 1800 3750 3350
Wire Wire Line
	3750 3350 6100 3350
Wire Wire Line
	3050 1825 3050 3450
Wire Wire Line
	3050 3450 6100 3450
Wire Wire Line
	2300 1825 2300 3550
Wire Wire Line
	2300 3550 6100 3550
$EndSCHEMATC
