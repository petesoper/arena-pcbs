/*
 * Confirm connections between pad and component device connections to the
 * probe pad board. 
 *
 * For every relay_tree.h array row, confirm that the right device
 * is connected by the relay board to the right pad and only the pad and
 * not any other device header position.
 *
 * The hardware used by this program is a raw Atmega328p (32 pin TQFP) with 
 * the (Arduino notation) pins listed in the arrays below connected to the
 * 14 position female header that normally plugs into the probe pad board.
 * A TTL serial to USB adapter is used to feed program output to a computer.
 *
 * TODO
 *  1) Use and arena LCD/pushbutton box for I/O depending on a jumper
 *  2) Use the pushbuttons to "continue to next test after error detected"
 *
 * NOTES
 *  1) This code expects the relay table to be an array of structures, each
 *     structure consisting of an unsigned short int (16 bits) and a five
 *     element array of unsigned char (8 bits). 
 *
 * Pete Soper
 * March 18, 2017
 *
 * version 0.50  U N T E S T E D
 */

#include <Arduino.h>
#include <Wire.h>
#include "../southeastcon2017/RelayTableGenerator/relayTable.h"

/*
 * Default I2C address of arena relay controller
 */

#define I2C_ADDR_RELAY 0x20

#define BIT(b) ( 1 << (b) )

/*
 * This array maps the five header positions that ordinarily connect to probe
 * pad board pads one through five to the Arduino pins connected to them with
 * this tester.
 */

//  Atmega pins for these Arduino pins: 12, 13, 14, 15, 16
uint8_t padToPin[] = {8,9,10,11,12};


/*
 * This array maps the six header positions ordinarily connecting to devices
 * on the probe pad board to the Arduino pins connected to them with this
 * tester. There are two diode pins: one for the regular, forward diode, and
 * one for the reverse diode. That is, in terms of combinations.{c,py}, the
 * array is ordered "W R C I D d".
 */

//  Atmega pins for these Arduino pins: 27, 10, 11, 25, 26, 27
uint8_t deviceToPin[] = {18,14,15,16,17,18};

const char* deviceToName[] = {"Wire", "Resistor", "Capacitor", "Inductor", "Diode", "DiodeReverse"};

const char* deviceToLetter[] = {"W", "R", "C", "I", "D", "d"};

/*
 * Unfortunately C does NOT have a binary print format, so we have
 *    to generate our own.  Admittedly these numbers could be printed
 *    in hex, but it is easier to visually see which relays should
 *    be energized in a binary format.
 * The Arduino binary number format begins with a capital B, while
 *    standard GCC format is 0b (does Arduino accept this too?)
 */

char *B16_format(uint16_t n) {
   static char buffer[32];
   char *cptr = buffer;

#if GENERATE_ARDUINO_B16_FORMAT
   *cptr++ = 'B';
#else
   *cptr++ = '0';
   *cptr++ = 'b';
#endif

   for (int i=0; i < 16; i++)
      *cptr++ = (n & BIT(15-i)) ? '1' : '0';

   *cptr++ = '\0';

   return buffer;
} // B16_format

/* Set the 16 relays to the state in the 16-bit relay parameter. The relays
 *    are attached to the Arduino via an I2C 16-bit port expander.
 * This code can still run without the I2C port expander attached - the
 *    write commands fail. This will allow testing of the code without a
 *    full setup, and the components can be hard-wired to the desired pads.
 */

void setRelays(uint16_t relayPattern)
{  
   Wire.beginTransmission(I2C_ADDR_RELAY);
   Wire.write(~(relayPattern & 0xFF));
   Wire.write(~((relayPattern >> 8) & 0xFF));
   Wire.endTransmission();
}

/*
 * Set all pins to input. Then set the given device pin to output high.
 * Confirm the expected pad pin is high and that no other pin (either pad or
 * device) is high.
 *
 * Return true for failure, false for success
 */

bool test(uint8_t device, uint8_t pad) {

  uint8_t outputPin = deviceToPin[device];
  uint8_t inputPin = padToPin[pad];

  for (uint8_t p = 0; p < sizeof(padToPin); p++) {
    pinMode(padToPin[p], INPUT);
  }

  for (uint8_t d = 0; d < sizeof(deviceToPin); d++) {
    pinMode(deviceToPin[d], INPUT);
  }

  pinMode(outputPin, OUTPUT);
  digitalWrite(outputPin, HIGH);

  for (uint8_t p = 0; p < sizeof(padToPin); p++) {
    if (padToPin[p] == inputPin) {
      if (! digitalRead(padToPin[p])) {
        return true;
      } 
    } else {
      if (digitalRead(padToPin[p])) {
        return true;
      } 
    }
    for (uint8_t d = 0; d < sizeof(deviceToPin); d++) {
      if (digitalRead(deviceToPin[d])) {
        return true;
      }
    }
  }

  return false;
} // test

void printRelays(uint16_t i, uint8_t d) {
  uint8_t e = (relay_tree[i].control_word << (15 - (d*3))) & 7;
  Serial.print("K");
  uint8_t v = 15 - (d*3);
  Serial.print(v++);
  if (e & 1) {
    Serial.print(" ON ");
  }
  Serial.print(v++);
  if (e & 2) {
    Serial.print(" ON ");
  }
  Serial.print(" K");
  Serial.print(v);
  if (e & 4) {
    Serial.print(" ON ");
  }
}

void setup() {
  Serial.begin(9600);
  Wire.begin();
} // setup

void loop() {

  /*
   * Iterate through the table of control words and per-device pad numbers
   *  1) Send the ones complement of the control word to the relay board via
   *     the I2C controller.
   *  2) Iterate through each of the five three bit fields of the control word
   *     while also indexing through each of the five pad numbers for each
   *     device controlled by the three bits:
   *    a) Fetch the expected pad number from the per-device element of the
   *       table row's array and decrement it to be zero-based.
   *    b) If the device number is 4 (diode) then add to it the high order bit
   *       of the control word. This will forced the number to 5 if the 
   *       reverse version of the diode is indicated.
   *   c)  Pass pad and device number to the test function and handle it's 
   *       pass/fail return. For a failure a pretend table header file line
   *       is put out and the state of the relays related to the failed 
   *       device to pad connection.
   */

  for (uint16_t i = 0; i < RELAY_TABLE_LENGTH; i++) {
    setRelays(relay_tree[i].control_word);
    // Try running this down to the point of failure
    delay(50);
    for (uint8_t d = 0; d < 5; d++) {
      uint8_t device = d;
      uint8_t pad = relay_tree[i].pads[d] - 1;
      if (device == 4) {
	if (relay_tree[i].control_word & 0x8000) {
	  device += 1;
        }
        if (test(device, pad)) {
          Serial.println("/*       R W  R  C  I  D   W  R  C  I  D   PAD 12345 */");
          Serial.print("     { ");
          Serial.print(B16_format(relay_tree[i].control_word));
          for (uint8_t dv = 0; dv < 5; dv++) {
	    Serial.print(", ");
            Serial.print(relay_tree[i].pads[dv]);
          }
          Serial.print(" }, // ");
          for (uint8_t pd = 1; pd < 6; pd++ ) {
            for (uint8_t c = 0; c < 5; c++ ) {
	      if (relay_tree[i].pads[c] == pd) {
		if ((c == 4) && (relay_tree[i].control_word & 0x8000)) {
		  Serial.print("d");
		} else {
		  Serial.print(deviceToLetter[c]);
                }
              }
            }
          }
          Serial.print(" - #");
	  Serial.println(i);
	  Serial.print(" Pad: ");
	  Serial.print(relay_tree[i].pads[d]);
	  Serial.print(" Device: ");
	  Serial.print(deviceToName[d]);
          Serial.print(" Relays: ");
          printRelays(i, d);
	  Serial.println();
          // HANG
          while (1)
            ;
        }
      }
    }
  }
}
