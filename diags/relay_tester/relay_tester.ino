/*
 * Confirm connections between pad and component device connections to the
 * probe pad board. 
 *
 * The hardware used by this program is a raw Atmega328p (32 pin TQFP) with 
 * the (Arduino notation) pins listed in the arrays below connected to the
 * 14 position female header that normally plugs into the probe pad board.
 * See ../../stage1-probe/relay-tester.* for the Eagle and schematic PDF of
 * the tester hardware. 
 *
 * A TTL serial to USB adapter is used to feed program output to a computer
 * at 57600 baud for fault details, pass numbers, etc. 
 *
 * The tester can be run standalone and the LEDs on the relay board used to 
 * identify the pattern being tested (voltmeter on the current limit resistors
 * to identify which device is getting a logic HIGH).
 *
 * The debug process goes like this:
 *  1) find the 300 ohm current limit resistor with 5v on it and note the
 *     device. 
 *  2) note the three relay/five relay states corresponding to the device
 *  3) confirm the wrong pad is seeing the 5 volts OR
 *      in the case of a "pad found HIGH" error, confirm that pad has about
 *      a volt on it, indicating a missing wire is causing an input to float
 *      and cause a bogus high logic level to be seen.
 *  4) study the relay-connection schematic and the relay board and connect
 *     the wiring error or add/reconnect the missing or loose wire.
 *
 * In all cases it pauses on a fault with the yellow LED on. The "SW" switch 
 * will advance past a fault to the next test and that will continue until 
 * another fault is detected. After all 2200 tests for the 88 entry table are 
 * performed, there is a five second pause, then it all repeats. One pass 
 * takes about three seconds.
 *
 * TODO
 * Support the arena LCD/pushbutton box for I/O if A3 is found low at startup.
 * button on A3. (don't forget a pullup on A3)
 *
 * Pete Soper
 * March 18, 2017
 * A few routines copies from 
 *  southeastcon2017/RelayTableGenerator/combinations.c
 *
 * version 1.01 used @ competition
 */

#include <Arduino.h>
#include <Wire.h>
#include <avr/pgmspace.h>
#include "../../../southeastcon2017/ArenaControl/relayTable.h"

/*
 * Default I2C address of arena relay controller
 */

#define I2C_ADDR_RELAY 0x20
#define LED 4
#define BUTTON 2
#define BUTTON_INT 0

#define BIT(b) ( 1 << (b) )

/*
 * Arduino pins A4/18 (Atmega pin 27) is used for I2C SDA
 * Arduino pins A5/19 (Atmega pin 28) is used for I2C SCL
 * Arduino pins A6/A7 connected to the "bus" going to the relay board I2C
 * controller instead of A1/A2 so that A1/A2 can be used for digital I/O.
 */

/*
 * This array maps the five header positions that ordinarily connect to probe
 * pad board pads one through five to the Arduino pins connected to them with
 * this tester. (NOTICE that what's published/external is one-origin, but these
 * data structures are all zero-origin)
 */

uint8_t padToPin[] = {15, 16, 5, 6, 7};

/*
 * Here's the table to keep us sane:
 *  Pad ONE-ORIGIN number            1   2   3   4   5
 *  Header pin number               11   9   7   5   3
 *  Arduino pin number              15  16   5   6   7
 *  328 (tqfp32) package pin number 24  25   9  10  11
 */

uint8_t padToAtmegaPin[] = {24, 25, 9, 10, 11};

/*
 * This array maps the six header positions ordinarily connecting to devices
 * on the probe pad board to the Arduino pins connected to them with this
 * tester. There are two diode pins: one for the regular, forward diode, and
 * one for the reverse diode. That is, in terms of combinations.{c,py}, the
 * array is ordered "W R C I D d".
 * But this is VERY TRICKY, because the WRCID convention wasn't communicated
 * (or didn't sink in) with the probe pad board designer, so the header
 * connector positions are actually in W, DR, R, I, C, R order. Got it? 
 */

//  Atmega pins for these Arduino pins: 32, 10, 11, 25, 26, 1
uint8_t deviceToPin[] = {13, 8, 9, 10, 11, 12};

const char* deviceToName[] = {"Wire", "Resistor", "Capacitor", "Inductor", "Diode", "DiodeReverse"};

/*
 *  Table in header connector position order
 *  Device ZERO-ORIGIN number        1   2   3   4   5   0
 *  Header pin number                2   4   6   8  10  12
 *  Device letter                    R   C   I   D  DR   W
 *  Arduino pin number              13   8   9  10  11  12
 *  328 (tqfp32) package pin number 17  12  13  14  15  16
 */

uint8_t deviceToAtmegaPin[] = {17, 12, 13, 14, 15, 16};

const char* deviceToLetter[] = {"W", "R", "C", "I", "D", "d"};

// True after switch press interrupt

static volatile uint8_t press = false;

/*
 * Switch press interrupt. Just set a flag to be noticed by mainline code.
 * The tester switch doesn't have any noticeable bounce.
 */

void switchPress() {
  press = true;
}

/*
 * (copied from Arena control code in southeastcon2017 repo)
 *
 * Unfortunately C does NOT have a binary print format, so we have
 *    to generate our own.  Admittedly these numbers could be printed
 *    in hex, but it is easier to visually see which relays should
 *    be energized in a binary format.
 * The Arduino binary number format begins with a capital B, while
 *    standard GCC format is 0b (does Arduino accept this too?)
 */

char *B16_format(uint16_t n) {
   static char buffer[32];
   char *cptr = buffer;

#if GENERATE_ARDUINO_B16_FORMAT
   *cptr++ = 'B';
#else
   *cptr++ = '0';
   *cptr++ = 'b';
#endif

   for (int i=0; i < 16; i++)
      *cptr++ = (n & BIT(15-i)) ? '1' : '0';

   *cptr++ = '\0';

   return buffer;
} // B16_format

/*
 * (copied from Arena control code in southeastcon2017 repo)
 *
 * Set the 16 relays to the state in the 16-bit relay parameter. The relays
 *    are attached to the Arduino via an I2C 16-bit port expander.
 * This code can still run without the I2C port expander attached - the
 *    write commands fail. This will allow testing of the code without a
 *    full setup, and the components can be hard-wired to the desired pads.
 */

void setRelays(uint16_t relayPattern)
{  
   Wire.beginTransmission(I2C_ADDR_RELAY);
   Wire.write(~(relayPattern & 0xFF));
   Wire.write(~((relayPattern >> 8) & 0xFF));
   Wire.endTransmission();
}

/*
 * Set all pad pins to input and device pins to output low. Then set the given 
 * device pin to high and return the state of the given pad pin.
 */

bool test(uint8_t pad, uint8_t device) {

  uint8_t outputPin = deviceToPin[device];

  for (uint8_t p = 0; p < sizeof(padToPin); p++) {
    pinMode(padToPin[p], INPUT);
  }

  for (uint8_t d = 0; d < sizeof(deviceToPin); d++) {
    pinMode(deviceToPin[d], OUTPUT);
    // For clarity (Arduino library gives you this for free)
    digitalWrite(deviceToPin[d], LOW);
  }

  pinMode(outputPin, OUTPUT);
  digitalWrite(outputPin, HIGH);

  return digitalRead(padToPin[pad]);
} // test

/*
 * Display the details of a device to pad connection
 */

void printRelays(uint16_t control_word, uint8_t p, uint8_t d) {
  Serial.print("Pad ");
  Serial.print(p+1);
  Serial.print(" Device ");
  Serial.print(d);
  Serial.print(" ");
  Serial.print(deviceToName[d]);
  uint8_t b = 12-(d*3);
  uint8_t e = (control_word >> b) & 7;
  b += 3;
  for (uint8_t m = 4; m > 0; m>>=1) {
    Serial.print(" K");
    Serial.print(b--);
    if (e & m) {
      Serial.print("-NO ");
    } else {
      Serial.print("-NC ");
    }
  }
  Serial.println();
}

/*
 * Yep
 */

void setup() {
  pinMode(BUTTON, INPUT_PULLUP);
  attachInterrupt(BUTTON_INT, switchPress, FALLING);
  pinMode(LED, OUTPUT);
  Wire.begin();
  // Why the weird ball speed? Early on the test cycles took FOREVER at low
  // baud rates as tons of verbiage spewed out, but the output was flakey
  // at 115k (long wires on the protoboard, sloppy wiring: N O I S Y)
  Serial.begin(57600);
  Serial.println("Starting");
} // setup

/*
 * Show a relay table row with similar format to that of the old relayTable.h format
 */
void printControlWord(uint16_t relayIndex, uint16_t relayPattern, uint8_t *pads) {
    Serial.println("          Relay Pattern   Pad Connections   Dev for each pad");
    Serial.println("         R W  R  C  I  D   W  R  C  I  D   PAD 12345");
    Serial.print("     { ");
    Serial.print(B16_format(relayPattern));
    for (uint8_t dv = 0; dv < 5; dv++) {
      Serial.print(", ");
      Serial.print(pads[dv]);
    }
    Serial.print(" }, // ");
    for (uint8_t pd = 1; pd < 6; pd++ ) {
      for (uint8_t c = 0; c < 5; c++ ) {
	if (pads[c] == pd) {
	  if ((c == 4) && (relayPattern & 0x8000)) {
	    Serial.print("d");
	  } else {
	    Serial.print(deviceToLetter[c]);
	  } // if c
	} // if relay_tree
      } // for c
    } // for pd
    Serial.print(" - #");
    Serial.println(relayIndex);
}

void loop() {
  // number of full passes through all table rows, devices, and pads
  static int count = 0;

  /*
   * Iterate through the table of control words and per-device pad numbers
   *  1) Send the ones complement of the control word to the relay board via
   *     the I2C controller.
   *  2) Decode the "turns count" word from the table (a sequence of device
   *     numbers for each pad) into a sequence of pad numbers for each device.
   *  3) Iterate through the devices (adjusting "diode" to "diode-reversed" if
   *     the control word high bit is set). Pass the expected pad and device
   *     to the test function that returns the state of the pad (the connection
   *     to the pad on the header connector the tester is plugged into). 
   *  3) Freeze on a failed test and wait for a switch press to advance to 
   *     next test.
   *  4) Pause a few seconds after all 88 patterns are tested, the repeat when
   *     this function called again.
   */

  for (uint16_t relayIndex = 0; relayIndex < RELAY_TABLE_LENGTH; relayIndex++) {
    // relay table stored in flash
    uint16_t relayPattern = pgm_read_word_near(&relayTable[relayIndex][0]);
    uint16_t turnPattern  = pgm_read_word_near(&relayTable[relayIndex][1]);

    uint8_t pads[5]; // The one-based pad number to be connected to a device (the index)
    uint16_t divisor = 10000;

    // turn the turns pattern into a five element array mapping the device 
    // number (as the index) into an expected pad number (one-origin).

    for (int i = 0; i < 5; i++) {
      int8_t device = ((turnPattern / divisor) % 10) - 1;
      pads[device] = i + 1;
      divisor /= 10;
    }

    // Set the relays and wait for them to switch
    setRelays(relayPattern);

    // 18ms is very reliable
    delay(18);

    // Iterate through the five device numbers (WRCID for 0-4)

    for (uint8_t d = 0; d < 5; d++) {
      uint8_t pad = pads[d] - 1;
      uint8_t dn = d;

      // Switch D to d if high pattern bit set for 0-5 device code

      if ((d == 4) && (relayPattern & 0x8000)) {
	dn += 1;
      }

      // Sense all pads

      for (uint8_t p = 0; p < 5; p++) {

        // If the expected pad, should see HIGH, else LOW

        uint8_t expected = p == pad ? HIGH: LOW;

        // test the device/pad combination

	if (test(p, dn) != expected) {
          if (expected) {
	    Serial.print(" FAIL - Target PAD ");
	    Serial.print(p+1);
            Serial.println(" found LOW");
          } else {
	    Serial.print(" FAIL - PAD ");
	    Serial.print(p+1);
            Serial.println(" found HIGH");
          }

          // Print out details

          printControlWord(relayIndex, relayPattern, pads);
          printRelays(relayIndex, pad, dn);

	  // HANG until switch press with indicator LED

          Serial.println("Press 'SW' to continue");
          digitalWrite(LED, HIGH);
	  while(!press)
	    ;
	  press = false;
          digitalWrite(LED, LOW);
	}
      }
    } // for d
  } // for relayIndex
  count += 1;


  Serial.print("Total Passes: ");
  Serial.println(count);
  delay(5000);
}
