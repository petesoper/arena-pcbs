/*
 * Demonstrate pin change interrupts
 * 
 * Uses the Southeastcon 2017 robot contest arena Controller/Display.
 * Display message on different display lines corresponding to button pushes.
 * Switch bounce and race conditions are ignored.
 *
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 Peter James Soper pete@soper.us
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <Arduino.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <SimplePinChange.h>

Sainsmart_I2CLCD lcd(0x27,20,4);  // I2C address 0x27, four lines of 20 chars

// Current switch state
volatile uint8_t s[4];

// Scan pins and set current switch state
void switch_press() {
  for (int i = 0; i < sizeof(s); i++) {
    s[i] = ! digitalRead(i + 14);
  }
}


// Put out a message on line 1-4
void message(int line, const char msg[]) {
  lcd.setCursor(0,line - 1);
  lcd.print(msg);
}

// Initialize pin modes, change interrupts, and LCD display

void setup() {
  lcd.init();
  lcd.backlight();
  message(1,"012345789");
  message(2,"012345789");
  message(3,"012345789");
  message(4,"012345789");
}
// Look for changes to the switches and display messages

void loop() {
  bool anyseen = false;
  for (int i = 0; i < sizeof(s); i++) {
    if (s[i]) {
      message(i+1,"Press");
      s[i] = 0;
      anyseen = true;
    }
  }
  if (anyseen) {
    delay(1000);
    lcd.clear();
  }
}
